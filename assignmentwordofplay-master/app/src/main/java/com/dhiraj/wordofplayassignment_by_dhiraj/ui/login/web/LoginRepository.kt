package com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.web

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class LoginRepository {

    fun loginUser(jsonObj:Map<String,String>,onResult: (isSuccess: Boolean, response: ApiResponse?) -> Unit) {

        try {
              LoginService.Factory.create().loginUser(jsonObj).enqueue(object : Callback<ApiResponse> {

                override fun onResponse(call: Call<ApiResponse>, response: Response<ApiResponse>) {
                    if (response != null && response.isSuccessful)
                        onResult(true, response.body())
                    else
                        onResult(false, null)
                }

                override fun onFailure(call: Call<ApiResponse>, t: Throwable) {
                    onResult(false, null)
                }
            })

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {
        private var INSTANCE: LoginRepository? = null
        fun getInstance() = INSTANCE
            ?: LoginRepository().also {
                INSTANCE = it
            }
    }
}