package com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.web

import com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.model.User
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface LoginService {

    @Headers("Content-Type: application/json")
    @POST("/login/")
    fun loginUser( @Body jsonBody: Map<String, String> ): Call<ApiResponse>


    object Factory {
        val gson = GsonBuilder()
            .enableComplexMapKeySerialization()
            .setLenient()
            .setPrettyPrinting()
            .create()

        private var okHttpClient = OkHttpClient.Builder()
            .addInterceptor(MockLoginInterceptor())
            .build()

        fun create(): LoginService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
            return retrofit.create(LoginService::class.java)
        }
    }

    companion object {
        const val BASE_URL = "https://api.github.com/"
    }
}