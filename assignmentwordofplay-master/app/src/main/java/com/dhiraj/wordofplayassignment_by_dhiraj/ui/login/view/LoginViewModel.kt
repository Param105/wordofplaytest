package com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.view

import android.util.Patterns
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.model.User
import com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.web.ApiResponse
import com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.web.LoginRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.regex.Matcher
import java.util.regex.Pattern


class LoginViewModel : BaseObservable() {

    var loginWebResponse = MutableLiveData<ApiResponse>()

    @get:Bindable
    var emailValid:Boolean = false
        set(emailValid) {
            field = emailValid
            notifyPropertyChanged(BR.emailValid)
        }

    @get:Bindable
    var passwordValid:Boolean = false
        set(passwordValid) {
            field = passwordValid
            notifyPropertyChanged(BR.passwordValid)
        }


    @get:Bindable
    var email: String? = null
        set(email) {
            field = email
            notifyPropertyChanged(BR.email)
            validateEmail(email)
        }

    private fun validateEmail(email: String?) {
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            errorEmail.set("Enter a valid email address")
        }else{
            emailValid = true
            errorEmail.set("")
        }

    }

    @get:Bindable
    var password: String? = null
        set(password) {
            field = password
            notifyPropertyChanged(BR.password)
            validatePassword(password)
        }


    /**
     * ^ # start-of-string
        (?=.*[0-9]) # a digit must occur at least once
        (?=.*[a-z]) # a lower case letter must occur at least once
        (?=.*[A-Z]) # an upper case letter must occur at least once
        (?=.*[@#$%^&+=]) # a special character must occur at least once replace with your special characters
        (?=\\S+$) # no whitespace allowed in the entire string
        .{8,} # anything, at least six places though
        $ # end-of-string
     */
    fun isValidPassword(password: String?,pass_pattern:String): Boolean {
        val FULL_PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$"
        val matcher: Matcher
        val pattern: Pattern = Pattern.compile(pass_pattern)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }

    private fun validatePassword(password: String?) {
        var errorString =""
        when{
            !isValidPassword(password,".{8,}")-> errorString = "Minimum 8 characters please"
            !isValidPassword(password,".{8,16}")-> errorString = "Maximum 16 characters please"
            !isValidPassword(password,"(.*[0-9].*)")-> errorString = "At least one number please"
            !isValidPassword(password,"(.*[a-z].*)")-> errorString = "At least one lowercase character please"
            !isValidPassword(password,"(.*[A-Z].*)")-> errorString = "At least one uppercase character please"
            !isValidPassword(password,"(.*[@#\$%^&+=].*)")-> errorString = "At least one special character please"
            else -> {
                errorString = "";
                passwordValid = true;
            }
        }
        errorPassword.set(errorString)
    }

    @get:Bindable
    var busy = 8
        set(busy) {
            field = busy
            notifyPropertyChanged(BR.busy)
        }
    val errorPassword = ObservableField<String?>()
    val errorEmail = ObservableField<String?>()
    private var userMutableLiveData: MutableLiveData<User>? =
        null

    val user: LiveData<User>
        get() {
            if (userMutableLiveData == null) {
                userMutableLiveData =
                    MutableLiveData()
            }
            return userMutableLiveData!!
        }

    fun onLoginClicked() {
        var jsonObj:HashMap<String,String> = HashMap();
        jsonObj.put(email!!,password!!);
        LoginRepository.getInstance().loginUser(jsonObj) { isSuccess:Boolean, response:ApiResponse? ->
            if (isSuccess) {
                CoroutineScope(Dispatchers.IO).launch {
                    loginWebResponse.postValue(response)
                }
            }
        };
    }

}

