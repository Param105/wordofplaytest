package com.dhiraj.wordofplayassignment_by_dhiraj.web

import com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.web.MockLoginInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitClient {
    private var retrofit: Retrofit? = null
    private const val BASE_URL = "https://jsonplaceholder.typicode.com"
    private var okHttpClient = OkHttpClient.Builder()
        .addInterceptor(MockLoginInterceptor())
        .build()

    val retrofitClient: Retrofit?
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()
            }
            return retrofit
        }
}