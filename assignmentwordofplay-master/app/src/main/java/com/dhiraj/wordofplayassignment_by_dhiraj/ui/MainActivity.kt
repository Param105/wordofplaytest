package com.dhiraj.wordofplayassignment_by_dhiraj.ui

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dhiraj.wordofplayassignment_by_dhiraj.R


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setCutomTheme()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun setCutomTheme(){
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        val savedTheme = sharedPref.getString(getString(R.string.saved_theme_name), getString(R.string.theme_one))
        when{
            savedTheme.equals(getString(R.string.theme_one)) -> setTheme(R.style.AppTheme1)
            savedTheme.equals(getString(R.string.theme_two)) -> setTheme(R.style.AppTheme2)
        }

    }
}
