package com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.dhiraj.wordofplayassignment_by_dhiraj.R
import com.dhiraj.wordofplayassignment_by_dhiraj.databinding.LoginFragmentBinding
import com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.model.User
import com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.web.ApiResponse

class LoginFragment : Fragment() {

    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var viewModel: LoginViewModel
    private lateinit var loginFragmentBinding: LoginFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loginFragmentBinding = LoginFragmentBinding.inflate(inflater, container, false)
        viewModel = LoginViewModel()
        loginFragmentBinding.loginViewModel = viewModel;
        setObserver()
        setThemeSwitcher()
        return loginFragmentBinding.root
    }

    private fun setThemeSwitcher() {
        var newTheme: String
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return

        val savedTheme = sharedPref.getString(getString(R.string.saved_theme_name), getString(R.string.theme_one))
        when{
            savedTheme.equals(getString(R.string.theme_one)) ->  {
                loginFragmentBinding.switchTheme.isChecked = false

            }
            savedTheme.equals(getString(R.string.theme_two)) -> {
                loginFragmentBinding.switchTheme.isChecked = true
            }
        }

        loginFragmentBinding.switchTheme.setOnClickListener(View.OnClickListener {
            if (!loginFragmentBinding.switchTheme.isChecked) {
                newTheme = getString(R.string.theme_one)
            } else {
                newTheme = getString(R.string.theme_two)
            }

            with (sharedPref.edit()) {
                putString(getString(R.string.saved_theme_name), newTheme)
                apply()
                 activity?.recreate()
            }
        })
        

    }

    private fun setObserver() {
        viewModel.loginWebResponse.observe(viewLifecycleOwner,
            Observer<ApiResponse> { response ->
             Toast.makeText(
                    activity?.applicationContext,
                    response.status.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}
