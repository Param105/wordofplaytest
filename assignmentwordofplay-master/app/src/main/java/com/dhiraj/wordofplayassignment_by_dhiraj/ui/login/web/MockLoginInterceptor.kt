package com.dhiraj.wordofplayassignment_by_dhiraj.ui.login.web

import okhttp3.*

import java.io.IOException


class MockLoginInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val responseString = createResponseBody(chain)
        return Response.Builder()
            .code(200)
            .message(responseString!!)
            .request(chain.request())
            .protocol(Protocol.HTTP_1_0)
            .body(
                ResponseBody.create(
                    MediaType.parse("application/json"),
                    responseString.toByteArray()
                )
            )
            .addHeader("content-type", "application/json")
            .build()
    }

    /**
     *
     * @param chain
     * @return
     */
    private fun createResponseBody(chain: Interceptor.Chain): String? {
        var responseString: String? = "{'status':'Mocked Success init'}"
        val uri: HttpUrl = chain.request().url()
        val path = uri.url().path
        if (path.matches(Regex("/login/"))) {
            responseString = getResponseString("login.json")
        }
        return responseString
    }

    private fun getResponseString(s: String): String? {
        return "{'status':'Mocked Success'}"
    }
}